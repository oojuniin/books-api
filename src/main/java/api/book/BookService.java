package api.book;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;

@Service
public class BookService implements Serializable {

    @Autowired
    private BookRepository repository;

    @Transactional
    public List<Book> findAll() {
        return repository.findAll();
    }

    @Transactional
    public Optional<Book> findById(long id) {
        return repository.findById(id);
    }

    @Transactional
    public Book save(Book book) {
        return repository.save(book);
    }

    @Transactional
    public void deleteById(long id) {
        repository.deleteById(id);
    }

}
